# Project description
This is a simple page implements in HTML with SCSS/CSS.
# Project objective
The main purpose of this project was to play a little bit with HTML and SCSS.
# Test it live
You can see this page on this [website](https://pskrajnyy.gitlab.io/developer_page_HTML_SCSS/)
